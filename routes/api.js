var express = require('express');
var router = express.Router();
var mailer = require('../api/mailer');
var instagram = require('../api/instagram');
var menu = require('../api/menu');

//post route for contact form submission
router.post('/contact',(req,res,next)=>{
    mailer.sendMail(req,res,next);
});

router.post('/catering',(req,res,next)=>{
    mailer.sendCateringMail(req,res,next);
});

//get route for instagram
router.get('/instagram',(req,res,next)=>{
    instagram.fetchPhotos(req,res,next);
});

//get route for menu categories
router.get('/menu/categories',(req,res,next)=>{
    menu.getMenuCategories(req,res,next);
});

//get route for menu items
router.get('/menu/items',(req,res,next)=>{
    menu.getMenuItems(req,res,next);
});

module.exports = router;