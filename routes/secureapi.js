const express = require('express');

const router = new express.Router();
const menu = require('../api/menu');

/* router.get('/dashboard', (req, res) => {
  res.status(200).json({
    message: "You're authorized to see this secret message."
  });
});
*/

router.post('/addItem',(req,res,next)=>{
  menu.addMenuItem(req,res);
});

router.post('/editItem',(req,res,next)=>{
  menu.editMenuItem(req,res);
});

router.post('/deleteItem',(req,res,next)=>{
  menu.deleteMenuItem(req,res);
});

module.exports = router;