var Menu = require('../models/menu');
var Category = require('../models/category');

module.exports.addMenuItem = (req,res)=>{
    let msg;
    let {name,type,addons,price,soyfree,glutenfree,description} = req.body;
    let newItem = new Menu({name,type,addons,price,soyfree,glutenfree,description});
    newItem.save((err)=>{
        if(err){
            console.error('Error while adding new menu item: '+err);
            if(err.code === 11000){
                msg = 'Error: Duplicate menu item exists please use the edit menu to adjust';
            }else{
                msg = err.errmessage;
            }
            res.statusCode = 400;
        }else{
            res.statusCode = 200;
            msg = 'Added new menu item: ' + name;
        }
        res.json(msg);
    });
}

module.exports.getMenuCategories = (req, res, next)=>{
    Category.find(function(err,categories){
        if(err){
            console.error('Error trying to get menu categories:'+err);
            next(err);
        }
        res.json({categories:categories});
    }).sort({name:'asc'});
}

module.exports.getMenuItems = (req, res, next)=>{
    Menu.find(function(err,items){
        if(err){
            console.error('Error trying to get menu items:'+err);
            next(err);
        }
        res.json({items:items});
    }).sort({name:'asc'});
}

module.exports.editMenuItem = (req,res)=>{
    let msg,
    selectedItem = req.body.selected;
    Menu.findByIdAndUpdate(selectedItem._id,selectedItem,(err)=>{
        if(err){
            console.error(`Error while updating menu item ${err}`);
            msg = err.errmessage;
            res.statusCode = 400;
        }else{
            res.statusCode = 200;
            msg = 'Updated menu item: ' + selectedItem.name;
        }
        res.json(msg);
    });
}

module.exports.deleteMenuItem = (req,res)=>{
    let msg,
    selectedItem = req.body.selected;
    Menu.findByIdAndRemove(selectedItem._id,(err,t)=>{
        if(err){
            console.error(`Error while deleting menu item ${err}`);
            msg = err.errmessage;
            res.statusCode = 400;
        }else{
            res.statusCode = 200;
            msg = `Deleted menu item: ${selectedItem.name}`;
        }
        res.json(msg);
    })
}