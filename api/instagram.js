var request = require('request');

module.exports.fetchPhotos = (req,res,next)=>{
    request(`https://api.instagram.com/v1/users/${process.env.INSTAGRAM_USER_ID}/media/recent/?access_token=${process.env.INSTAGRAM_ACCESS_TOKEN}&count=${req.query.count}`, function (err, response, body) {
        if(err){
            console.error(`error fetching instagram feed: ${err}`);
            next(err);
        }
        body = JSON.parse(body);
        let cleanData = [];
        body.data.map((item)=>{
            cleanData.push({image:item.images.thumbnail.url,url:item.link,caption: item.caption.text});
        });
        res.json(cleanData);
    });
}