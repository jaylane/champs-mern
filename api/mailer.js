var Contact = require('../models/contact');
var helpers = require('../helpers');
var mailgun = require('mailgun-js')({apiKey: process.env.MAILGUN_API_KEY, domain: process.env.MAILGUN_DOMAIN});

module.exports.sendMail = (req,res,next)=>{
    let {name,email,message} = req.body,
    contact = new Contact({name,email,message});

    contact.save((err)=>{
        if(err){
            console.error(`Error while saving contact to database: ${err}`);
            next(err);
        }
        console.log('Contact saved to database');
    });

    let createdEmail = helpers.createEmail({name,email,message}),
    data = {
        from: `${name} <${email}>`,
        to: 'jason.j.lane@gmail.com,info@champsdiner.com',
        subject: `Contact form submission from ChampsDiner.com`,
        text: `Contact form submission from ChampsDiner.com \nName: ${name} \nEmail: ${email}\n Message: ${message}`,
        html: createdEmail
    };

    mailgun.messages().send(data, (err,body)=>{
        if(err){
            console.error(`Error while sending contact email with mailgun: ${err}`);
            next(err);
        }
        console.log(`Successful mailgun response: ${body.message}`);
        res.statusCode = 200;
        res.send('Email Successfully Sent');
    });
}

module.exports.sendCateringMail = (req,res,next)=>{
    let {name,email,message,phone,eventType,eventSize,eventAddress,eventDate,eventMethod} = req.body,
    contact = new Contact({name,email,message});

    contact.save((err)=>{
        if(err){
            console.error(`Error while saving contact to database: ${err}`);
            next(err);
        }
        console.log('Contact saved to database');
    });

    let createdEmail = helpers.createCateringEmail({name,email,message,phone,eventType,eventSize,eventAddress,eventDate,eventMethod}),
    data = {
        from: `${name} <${email}>`,
        to: 'jason.j.lane@gmail.com,champsempireevents@gmail.com',
        subject: `Catering Request submission from ChampsDiner.com`,
        text: `Catering Request form submission from ChampsDiner.com \nName: ${name} \nEmail: ${email}\n Message: ${message}`,
        html: createdEmail
    };

    mailgun.messages().send(data, (err,body)=>{
        if(err){
            console.error(`Error while sending contact email with mailgun: ${err}`);
            next(err);
        }
        console.log(`Successful mailgun response: ${body.message}`);
        res.statusCode = 200;
        res.send('Email Successfully Sent');
    });
}