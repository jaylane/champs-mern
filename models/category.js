var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var catSchema = new Schema({
    name: String
}, {collection: 'category'});

module.exports = mongoose.model('Category', catSchema);