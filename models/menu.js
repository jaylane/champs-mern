var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var menuSchema = new Schema({
    name: {type:String, required:true, unique:true},
    type: {type:String, required:true},
    addons: [{name: String, price: Number}],
    price: {type:Number, currency:"USD", required: true},
    soyfree: {type: Boolean, default: false},
    glutenfree: {type: Boolean, default: false},
    description: String
},{timestamps:true});

module.exports = mongoose.model('Menu', menuSchema);