//modules
require('dotenv').config({path: './champs.env'});
var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
const passport = require('passport');
const userSchema = require('./models/user');

//connect to mongodb 
mongoose.connect(process.env.MONGO_CONNECT,{useMongoClient:true},()=> console.log('connected to champs mongodb instance'));
mongoose.Promise = global.Promise;

//initialize express
var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//initialize passport middleware
app.use(passport.initialize());


//passport strategies
const localSignupStrategy = require('./passport/local-signup');
const localLoginStrategy = require('./passport/local-login');
passport.use('local-signup', localSignupStrategy);
passport.use('local-login', localLoginStrategy);

// pass the authenticaion checker middleware
const authCheckMiddleware = require('./middleware/auth-check');
app.use('/secure/api', authCheckMiddleware);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


app.use(logger('dev'));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'client','build')));

// routes
const authRoutes = require('./routes/auth');
const apiRoutes = require('./routes/api');
const secureApiRoutes = require('./routes/secureapi');
app.use('/auth', authRoutes);
app.use('/api', apiRoutes);
app.use('/secure/api', secureApiRoutes);

//Serve built files from client folder on all get requests
app.get('*', function(req, res, next) {
  res.sendFile(path.join(__dirname,'client','build','index.html'));
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
