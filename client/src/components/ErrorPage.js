import React from 'react';
import PropTypes from 'prop-types';

const ErrorPage = (props)=>{
    return(
        <div className="error-wrapper">
            <h1 className="error-message">Error</h1>
            <h2>{props.errorMessage}</h2>
        </div>
    );
}


ErrorPage.propTypes = {
    errorMessage: PropTypes.string.isRequired
}
export default ErrorPage;