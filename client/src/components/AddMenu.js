import React,{Component} from 'react';
var axios = require('axios');

export default class AddMenu extends Component{
    constructor(props){
        super(props);
        this.state = {error:''};
    }
    handleSubmit = (e)=>{
        e.preventDefault();
        this.setState({error: ''});
        
        let name = this.name.value.trim(),
        type = this.type.value.trim(),
        price = this.price.value.trim(),
        addons = this.addons.value,
        soyfree = this.soyfree.value,
        glutenfree = this.glutenfree.value,
        description = this.description.value.trim(),
        token = localStorage.getItem('token') || '';

        if(parseFloat(price) < .01){
            this.setState({error:'Please set an item price'});
            return false;
        }

        if(addons.length > 0 && addons.indexOf(':') < 0){
            this.setState({error:'Incorrect formatting for Addons'});
            return false;
        }

        let validated = true,
        msg;

        if(addons.length === 0){
            addons = [];
        }else{
            addons = addons.split(',').map((e)=>{return e.split(':')});;
            for(let addon of addons){
            if((addon.length > 2 || addon.length < 2)){
                msg = 'Incorrect formatting for Addons';
                validated = false;
                break;
            }else if(typeof addon[1] === 'undefined' || addon[1].length === 0){
                msg = 'Tried to set a non numerical value for addon price';
                validated = false;
                break;
            }else if(isNaN(parseFloat(addon[1]))){
                msg = 'Tried to set a non numerical value for addon price';
                validated = false;
                break;
            }  
            };
        }

    

        if(validated === true){
            if(addons.length > 0){
                addons = addons.map(function(el,i){
                    return el.map(function(ele,j){
                        return j === 0 ? {name:ele.trim()} : {price:ele.trim()};
                    });
                }).map((el)=>{
                    return Object.assign(el[0],el[1]);
                });
            }
        
            axios.post('/secure/api/addItem',{
                name,type,price,addons,soyfree,glutenfree,description
            },
            {
                headers: {Authorization: "Bearer "+token}
            })
            .then((res)=>{
                this.setState({error:res.data});
                this.props.menuCount();
            }).catch((err)=>{
                console.error(err);
                this.setState({error:err.response.data});
            });        
        
            this.form.reset();
        }else{
            this.setState({error:msg});
        }    
    }
    render(){
        return(
            <div className="about-wrapper">
                <h2 className="sub-page-heading">Add Menu Item</h2>
                {this.state.error ? <p className="error">{this.state.error}</p> : null}
                <form className="add-menu-item-form" ref={(form)=>{this.form = form}}>
                    <ul className="menu-form-list">
                        <li className="menu-item">
                            <label htmlFor="name">Name:</label>
                            <input type="text" name="name" ref={(input)=> this.name = input} required />   
                        </li>
                        <li className="menu-item">
                            <label htmlFor="type">Type:</label>
                            <select name="type" ref={(input)=> this.type = input} required>
                                {this.props.categories.map(cat => <option key={cat._id} value={cat.name}>{cat.name}</option>)}
                            </select>      
                        </li>
                        <li className="menu-item">
                            <label htmlFor="price">Price:</label>
                            <input type="number" name="price" ref={(input)=> this.price = input} required step="0.01" min="0" defaultValue="0.00" />    
                        </li>
                        <li className="menu-item">
                            <label htmlFor="addons">Addons (Name:Price):</label>
                            <input type="text" name="addons" ref={(input)=> this.addons = input} />    
                        </li>
                        <li className="menu-item">
                            <label htmlFor="soyfree">Soy Free:</label>
                            <select name="soyfree" required ref={(input)=> this.soyfree = input}>
                                <option value={false} defaultValue>No</option>
                                <option value={true}>Yes</option>
                            </select>     
                        </li>
                        <li className="menu-item">
                            <label htmlFor="glutenfree">Gluten Free:</label>
                            <select name="glutenfree" required ref={(input)=> this.glutenfree = input}>
                                <option value={false} defaultValue>No</option>
                                <option value={true}>Yes</option>
                            </select>     
                        </li>
                        <li className="menu-item">
                            <label htmlFor="description">Description:</label>
                            <textarea name="description" id="add-menu-desc" rows="4" cols="35" ref={(input)=> this.description = input }/>  
                        </li>
                        <li className="menu-item add-menu-submit">
                            <input type="submit" value="Add Item" onClick={this.handleSubmit} />  
                        </li>
                    </ul>
                </form>
            </div>
        )
    }
}
