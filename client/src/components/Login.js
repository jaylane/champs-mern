import React, {Component} from 'react';
import 'whatwg-fetch';
import Auth from '../modules/Auth';
import {Redirect} from 'react-router-dom';

export default class Login extends Component{
    constructor(props,context){
        super(props,context);
        const storedMessage = localStorage.getItem('successMessage');
        let successMessage = '';

        if(storedMessage){
            successMessage = storedMessage;
            localStorage.removeItem('successMessage');
        }

        this.state = {
            errors: '',
            successMessage,
            fireRedirect: false
        }
    }
    handleLogin = (e)=>{
        e.preventDefault();
        let email = this.email.value,
        password = this.password.value;
        
        fetch('/auth/login', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              email,password
            })
        }).then((res)=>{
          return res.json();
        })
        .then((json)=>{
            if(json.success){
                Auth.authenticateUser(json.token);
                localStorage.setItem('successMessage',json.message);
                this.setState({fireRedirect:true});
            }else{
                let errors = json.message;
                this.setState({errors});
            }
        })
        .catch((err)=>{
            const errors = err ? err : '';
            console.error(err);
            this.setState({errors});
        });
        this.form.reset();
    }
    render(){
        return(
            <div className="login-container">
            <h1 className="page-heading">Administrator Login</h1>
                {this.state.errors ? <h4 className="error-message">{this.state.errors.toString()}</h4> : null}
                <form className="login-form" ref={(form)=>{this.form = form}}>
                    <ul className="flex-outer">
                        <li className="email-item">
                            <label htmlFor="email">Email:</label>
                            <input autoComplete="nope" type="email" name="email" ref={(input)=> this.email = input} required />  
                        </li>
                        <li className="password-item">
                            <label htmlFor="password">Message:</label>
                            <input autoComplete="nope" type="password" rows="6" name="password" ref={(input)=> this.password = input} required />
                        </li>
                        <li>
                            <button type="submit" onClick={this.handleLogin}>Login</button>
                        </li>
                    </ul>
                </form>
                {this.state.fireRedirect && <Redirect to={'/admin'} />}
            </div>
        )
    }
}