import React, {Component} from 'react';
import Helpers from '../modules/Helpers';
import FontAwesome from 'react-fontawesome';
var axios = require('axios');

export default class EditMenu extends Component{
    constructor(props){
        super(props);
        this.state = {selected:{},error:'',stringAddons:'',displayConfirm:false};
    }
    componentDidMount(){
        let stringAddons = '',
        selected;

        if(this.props.items.length > 0){
            selected = this.props.items[0];
            if(typeof selected.addons === 'object' && selected.addons.length > 0){
                stringAddons = Helpers.menu.stringifyAddons(selected.addons);
            }
            this.setState({selected:selected,stringAddons:stringAddons});
        }else{
            this.setState({error: 'No Menu Items Found'});
        }

    }
    componentWillReceiveProps(nextProps){
        if(this.props !== nextProps){
            this.setState({selected:this.props.items[0]});
        }
    }
    handleItemSelect = (event)=>{
        let selected = this.props.items[event.target.selectedIndex],
        stringAddons = '';
        if(typeof selected.addons === 'object' && selected.addons.length > 0){
            stringAddons = Helpers.menu.stringifyAddons(selected.addons);
        }
        this.setState({selected:selected,stringAddons:stringAddons});
    }
    handleAddons = (event)=>{
        let updatedValue = event.target.value;
        this.setState({stringAddons:updatedValue});
    }
    handleUpdate = (event)=>{
        let selected = this.state.selected,
        property = event.target.name,
        updatedValue = event.target.value;
        if(property === 'soyfree' || property === 'glutenfree'){
           if(typeof updatedValue === 'string'){
               updatedValue = JSON.parse(event.target.value);
           }
        }
        if(typeof updatedValue === 'string'){
            updatedValue = updatedValue.trim();
        }
        selected[property] = updatedValue;
        this.setState({selected:selected});
    }
    confirmDelete = ()=>{
        this.setState({displayConfirm:true});
    }
    handleSubmit = (event)=>{
        event.preventDefault();
        this.setState({error: ''});
        let token = localStorage.getItem('token') || '',
        selected = this.state.selected,
        validated = true,
        msg,
        addons;
        
        if(this.state.stringAddons.length > 0 && this.state.stringAddons.indexOf(':') < 0){
            this.setState({error:'Incorrect formatting for Addons'});
            return false;
        }

        if(this.state.stringAddons.length === 0){
            addons = [];
        }else{
            addons = Helpers.menu.destructureAddons(this.state.stringAddons);
            for(let addon of addons){
                if((addon.length > 2 || addon.length < 2)){
                    msg = 'Incorrect formatting for Addons';
                    validated = false;
                    break;
                }else if(typeof addon[1] === 'undefined' || addon[1].length === 0){
                    msg = 'Tried to set a non numerical value for addon price';
                    validated = false;
                    break;
                }else if(isNaN(parseFloat(addon[1]))){
                    msg = 'Tried to set a non numerical value for addon price';
                    validated = false;
                    break;
                }  
            };

        }
        

        if(validated === true){
            if(addons.length > 0){
                addons = addons.map(function(el,i){
                    return el.map(function(ele,j){
                        return j === 0 ? {name:ele.trim()} : {price:ele.trim()};
                    });
                }).map((el)=>{
                    return Object.assign(el[0],el[1]);
                });
            }
        
            selected.addons = addons;
        
            axios
            .post('/secure/api/editItem',
                {selected:selected},
                {headers: {Authorization: "Bearer "+token}}
            )
            .then(res =>{this.setState({error:res.data})})
            .catch((err)=>{
                console.error(err);
                this.setState({error:err.response.data});
            });
        }else{
            this.setState({error:msg});
        }
    }
    handleCancel = ()=>{
        this.setState({displayConfirm:false})
    }
    handleDelete = ()=>{
        let dataObject = {_id:this.state.selected._id,name:this.state.selected.name},
        token = localStorage.getItem('token') || '';
        axios
            .post('/secure/api/deleteItem',
                {selected:dataObject},
                {headers: {Authorization: "Bearer "+token}}
            )
            .then(res =>{this.setState({error:res.data,displayConfirm:false}); this.props.menuCount();})
            .catch((err)=>{
                console.error(err);
                this.setState({error:err.response.data,displayConfirm:false});
            });
    }
    render(){
        let soyFreeOptions;
        soyFreeOptions = [<option key={1} value={this.state.selected.soyfree}>{this.state.selected.soyfree ? 'Yes' : 'No'}</option>,
        <option key={2} value={!this.state.selected.soyfree}>{this.state.selected.soyfree ? 'No' : 'Yes'}</option>];

        let glutenFreeOptions;
        glutenFreeOptions = [<option key={1} value={this.state.selected.glutenfree}>{this.state.selected.glutenfree ? 'Yes' : 'No'}</option>,
        <option key={2} value={!this.state.selected.glutenfree}>{this.state.selected.glutenfree ? 'No' : 'Yes'}</option>];


        return(
            <div className="about-wrapper">
                <h2 className="sub-page-heading">Edit Menu Item</h2>
                {this.state.error ? <p className="error">{this.state.error}</p> : null}
                {this.state.displayConfirm ? <p className="error conf">Are you sure you want to delete: {this.state.selected.name}? <FontAwesome name="check-circle" className="confirm" onClick={this.handleDelete}/> / <FontAwesome name="times-circle" className="cancel" onClick={this.handleCancel} /></p> : null}
                <ul className="menu-form-list">
                    <li className="menu-item full-row">
                        <label htmlFor="item" className="select-item-label">{this.state.selected ? <FontAwesome name="ban" className="delete-item" onClick={this.confirmDelete} /> : null} Select Item:</label>   
                        <select className="select-item" name="item" ref={(input)=> this.item = input} required onChange={this.handleItemSelect} value={this.state.selected.name}>
                            {this.props.items.map((item,i) => <option key={i} value={item.name}>{item.name}</option>)}
                        </select>  
                    </li>
                    <li className="menu-item">
                        <label htmlFor="name">Name:</label>
                        <input type="text" name="name" ref={(input)=> this.name = input} value={this.state.selected.name ? this.state.selected.name : ''} onChange={this.handleUpdate} required />   
                    </li>
                    <li className="menu-item">
                        <label htmlFor="type">Type:</label>
                        <select name="type" ref={(input)=> this.type = input} required value={this.state.selected.type ? this.state.selected.type : ''} onChange={this.handleUpdate}>
                            {this.props.categories.map((cat,i) => <option key={i} value={cat.name}>{cat.name}</option>)}
                        </select>      
                    </li>
                    <li className="menu-item">
                        <label htmlFor="price">Price:</label>
                        <input type="number" name="price" ref={(input)=> this.price = input} required step="0.01" min="0" value={this.state.selected.price ? parseFloat(this.state.selected.price).toFixed(2) : '0.00'} onChange={this.handleUpdate} />    
                    </li>
                    <li className="menu-item">
                        <label htmlFor="addons">Addons (Name:Price):</label>
                        <input type="text" name="addons" ref={(input)=> this.addons = input} value={this.state.stringAddons} onChange={this.handleAddons}/>    
                    </li>
                    <li className="menu-item">
                        <label htmlFor="soyfree">Soy Free:</label>
                        <select name="soyfree" required ref={(input)=> this.soyfree = input} onChange={this.handleUpdate} value={this.state.selected.soyfree}>
                            {soyFreeOptions}
                        </select>     
                    </li>
                    <li className="menu-item">
                        <label htmlFor="glutenfree">Gluten Free:</label>
                        <select name="glutenfree" required ref={(input)=> this.glutenfree = input} onChange={this.handleUpdate} value={this.state.selected.glutenfree}>
                            {glutenFreeOptions}
                        </select>     
                    </li>
                    <li className="menu-item">
                        <label htmlFor="description">Description:</label>
                        <textarea name="description" id="add-menu-desc" rows="4" cols="35" ref={(input)=> this.description = input } onChange={this.handleUpdate} value={this.state.selected.description ? this.state.selected.description : ''}/>  
                    </li>
                    <li className="menu-item add-menu-submit">
                        <input type="submit" value="Update" onClick={this.handleSubmit} />  
                    </li>
                </ul>
            </div>
        )
    }
}