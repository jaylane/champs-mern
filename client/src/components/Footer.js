import React from 'react';
import FontAwesome from 'react-fontawesome';

const Footer = ()=>{
    let year = new Date().getFullYear();

    return(
        <footer className="footer-wrapper">
            <p className="footer-text">made with <FontAwesome name="heart-o"/> by <a href="http://www.jay-lane.com" rel="noopener noreferrer" target="_blank">Jay Lane</a> &copy;Champs Diner {year}</p>
        </footer>
    );
}

export default Footer;