import React, {Component} from 'react';
import 'whatwg-fetch';

export default class Contact extends Component{
    constructor(props){
        super(props);
        this.state = {
            showForm: true
        }
    }
    handleSubmit = (e)=>{
        e.preventDefault();
        let name = this.name.value,
        email = this.email.value,
        message = this.message.value;

        fetch('/api/contact', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              name,email,message
            })
        });
        this.form.reset();
        this.setState({...this.state,showForm:false});
    }
    render(){
        let body;
        if(this.state.showForm){
            body = <form className="contact-form" ref={(form)=>{this.form = form}}>
            <ul className="flex-outer">
                <li className="name-item">
                    <label htmlFor="name">Your Name:</label>
                    <input autoComplete="name" type="text" name="name" ref={(input)=> this.name = input} required />    
                </li>
                <li className="email-item">
                    <label htmlFor="email">Email:</label>
                    <input autoComplete="email" type="email" name="email" ref={(input)=> this.email = input} required />  
                </li>
                <li className="message-item">
                    <label htmlFor="message">Message:</label>
                    <textarea rows="6" type="textarea" name="message" ref={(input)=> this.message = input} required />
                </li>
                <li>
                    <button type="submit" onClick={this.handleSubmit}>Submit</button>
                </li>
            </ul>
        </form>;
        }else{
            body = <h3 className="contact-thanks">Thanks for contacting us! We'll get back to you as soon as possible.</h3>;
        }
        return(
            <div className="contact-wrapper">
                <h1 className="page-heading">Contact Us</h1>
                {body}
            </div>
        );
    }
}