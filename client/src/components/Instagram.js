import React, {Component} from 'react';
import PropTypes from 'prop-types';
import 'whatwg-fetch';

export default class Instagram extends Component{
    constructor(props){
        super(props);
        this.state = {instagramData: [], loading:true};
    }
    componentDidMount(){
        fetch(`/api/instagram?count=${this.props.count}`)
            .then(response => {
                return response.json()
            })
            .then(json => {
                this.setState({...this.state,instagramData:json,loading:false});
            }).catch(ex =>{
                console.error('parsing failed', ex);
                this.setState({...this.state,loading:false});
        });
    }
    renderLoading(){
        return <div className="loader">Loading...</div>
    }
    renderError(){
        return <h2>Sorry and error occured while loading instagram content.</h2>
    }
    renderInstagram(){
        let photos = this.state.instagramData.map((item, i)=>{
            return <a key={i} href={item.url} rel="noopener noreferrer" target="_blank"><img src={item.image} alt={item.caption} /></a>;
        });
        return (
            <div className="instagram-container">
                <h2>Follow Us on Instagram <a href="https://www.instagram.com/champsdiner/" target="_blank" rel="noopener noreferrer">@champsdiner</a></h2>
                <div className="instagram-wrapper">
                    {photos}
                </div>
            </div>
        )
    }
    render(){
        if (this.state.loading) {
            return this.renderLoading();
        } 
        else if (this.state.instagramData) {
            return this.renderInstagram();
        } 
        else {
            return this.renderError();
        }
    }
}

Instagram.propTypes = {
    count: PropTypes.number
};