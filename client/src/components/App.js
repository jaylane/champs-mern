import React, { Component } from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import '../css/App.css';
import Banner from './Banner';
import Nav from './Nav';
import {CSSTransition, TransitionGroup} from 'react-transition-group';
import About from './About';
import Contact from './Contact';
import Footer from './Footer';
import Home from './Home';
import Login from './Login';
import Auth from '../modules/Auth';
import ErrorPage from './ErrorPage';
import CateringForm from './CateringForm';
import Admin from './Admin';
import Faq from './Faq';
import Menu from './Menu';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Banner />
          <Nav />
          <main>
            <Route render={({location})=>(
              <TransitionGroup className="trans-wrap">
                  <CSSTransition key={location.pathname.split('/')[1]} timeout={{enter:1000, exit: 300}} classNames="fade">
                    <Switch location={location}>
                      <Route exact path="/" component={Home} />
                      <Route path="/about" component={About} />
                      <Route path="/faq" component={Faq} />
                      <Route path="/contact" component={Contact} />
                      <Route path="/menu" component={Menu} />
                      <Route path="/login" component={Login} />
                      <Route path="/catering" component={CateringForm} />
                      <Route path="/admin" render={(props)=> Auth.isUserAuthenticated() ? <Admin match={props.match} /> : <ErrorPage errorMessage="401 Unauthorized" />}/> 
                      <Route path="/logout" render={()=>{Auth.deauthenticateUser(); return <Home />}}  />
                      <Route render={()=><ErrorPage errorMessage="404 Page Not Found"/>} />
                    </Switch>
                  </CSSTransition>
                  </TransitionGroup>
              )} /> 
          </main>
          <Footer />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
