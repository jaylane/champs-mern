import React from 'react';

const Faq = ()=>{
    return(
        <div className="about-wrapper">
           <h1 className="page-heading">FAQ</h1>
           <h2>Driving</h2>
           <p className="line-height">We are located on a residential block. There are no parking lots or garages in the vicinity - street parking only. Parking can be tough! We recommend taking public transit or a car service.</p>
           <h2>Public Transportation</h2> 
           <p className="line-height">Champs is one block away from the Montrose Avenue stop on the L train, approximately a 15-minute walk from the Broadway stop on the G train, and approximately a 15-minute walk from both the Lorimer and Flushing stops on the JMZ line.</p>
           <p className="line-height">The nearest bus stop is two blocks away on the B43 at Graham Avenue and Scholes Street.</p>
           <h3>Takeout and Delivery</h3>
           <p className="line-height">Orders can be placed online through the below services. Please note that takeout and delivery may become unavailable during peak hours.</p>
           <p className="line-height"><a href="https://www.grubhub.com/restaurant/champs-197-meserole-st-brooklyn/279740" target="_blank" rel="noopener noreferrer">GrubHub</a><br />Daily, 10am-9:30pm</p>
           <p className="line-height"><a href="https://www.doordash.com/store/champs-diner-brooklyn-11168/" target="_blank" rel="noopener noreferrer">DoorDash</a><br />Monday - Thursday, 9am-11pm<br />Friday, 9am-7pm<br/>Saturday - Sunday, 9am-10am</p>
           <p className="line-height"><a href="https://www.amazon.com/restaurants/champs-diner-brooklyn/d/B01F2P0YL2?ref_=amzrst_pnr_ccp_b_B01F2P0YL2_4" target="_blank" rel="noopener noreferrer">Amazon Restaurants</a><br/>Daily, 9am-4:30pm, 5pm-11:30pm</p>
           <h2>What to expect when you arrive</h2>
           <p className="line-height"><b>No reservations or call aheads.</b> Walk-ins only. We never seat incomplete parties, even if tables are available.</p>
           <p className="line-height"><b>For live wait estimates and to check your place on the list, look us up with <a href="http://nowait.com/guests/" target="_blank" rel="noopener noreferrer">Nowait</a></b></p>
           <p className="line-height">In the event that you arrive and there is a waiting list, our host will be able to take your phone number and notify you via text message when your table is ready.</p>
           <p className="line-height">There is no waiting area available inside the restaurant. We ask that all guests who are waiting for tables or carryout orders to do so outside year round. We ask this to adhere to fire codes and provide a clear, accessible path, as well as a courtesy to our staff and other guests.</p>
           <p className="line-height">No Wi-Fi available.</p>
           <p className="line-height">Our last table seating is at 11:40pm daily.</p>
           <h2>Seating policies</h2>
           <p className="line-height">Due to our small size, we have a few necessary seating policies that we adhere to at all times.</p>
           <p className="line-height">We never seat incomplete parties, even if tables are available. Your whole party must be together and on time to be seated. Late arrivals and joiners may be asked to sit separately, especially if we are operating on a wait.</p>
           <p className="line-height">All seats in the restaurant are for dine-in customers only, and carryout orders may not be consumed at tables or the counter.</p>
           <h2>Waiting for a table</h2>
           <p className="line-height">If you plan on coming to Champs on the weekend, you should also plan to wait for your table.</p>
           <p className="line-height">We can accommodate group sizes of up to 7 people.</p>
           <p className="line-height">For parties of two on weekends, wait times can vary from 20-90 minutes. For groups of 3 or more on weekends, wait times can vary from 45-120 minutes.</p>
           <p className="line-height">In the event that you arrive and there is a waiting list, our host will be able to take your phone number and notify you via text message when your table is ready.</p>
           <p className="line-height">We allow a 10-minute grace period for you to return. Everyone in your party must be present in order to be seated. Should you not respond or arrive within that window, you will forfeit your place in line.</p>
        </div>
    );
}

export default Faq