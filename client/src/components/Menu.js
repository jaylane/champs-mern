import React,{Component} from 'react';
import FontAwesome from 'react-fontawesome';
var axios = require('axios');

export default class Menu extends Component{
    constructor(props){
        super(props);
        this.state = {
            items: [], categories:[], error: ''
        }
    }
    getMenuCategories = ()=>{
        let url = '/api/menu/categories';
        return axios.get(url);
    }
    getMenuItems = ()=>{
        let url = '/api/menu/items';
        return axios.get(url);
    }
    fetchData = ()=>{
        axios.all([this.getMenuCategories(),this.getMenuItems()])
        .then(axios.spread((cats,items)=>{
            if(items.data.items.length > 0 && cats.data.categories.length > 0){ 
                this.setState({items:items.data.items,categories:cats.data.categories});
            }else{
                this.setState({error:'No menu items found'});
            }
        }));
    }
    componentDidMount(){
        this.fetchData();
    }
    renderMenuItemsByCategory = (category)=>{
        return this.state.items.filter((item)=>{
            return item.type === category
        })
        .map((item,i)=>{
            return <ul key={i}><li className="menu-header">{item.name} ${item.price} {item.glutenfree === true ? <FontAwesome name="circle" className="gluten-free" /> : null} {item.soyfree === true ? <FontAwesome name="times-circle" className="soy-free"/> : null}</li>{item.description.length > 0 ? <li className="menu-desc">{item.description}</li> : null} {item.addons.length > 0 ? <li className="menu-addons">{item.addons.map((addon,i)=>{let concatAdds, addPrefix; if(i === item.addons.length - 1){concatAdds = ''}else{concatAdds = ', '} if(addon.name.toLowerCase().includes('cup') || addon.name.toLowerCase().includes('bowl') || addon.name.toLowerCase().includes('large') || addon.name.toLowerCase().includes('small') || addon.name.toLowerCase().includes('sub')){addPrefix = ''}else{addPrefix = 'Add '} return `${addPrefix}${addon.name} +${parseFloat(addon.price).toFixed(2)}${concatAdds}`})}</li> : null}</ul>
        })
    }
    renderDrinkItems = ()=>{
        return this.state.items.filter((item)=>{
            return item.type === 'Drinks'
        })
        .map((item,i)=>{
            return <ul key={i}><li className="menu-header">{item.name} {item.name === 'Draft Soda' || item.name === 'Tea' ? `$${item.price.toFixed(2)}` : null}</li>{item.description.length > 0 ? <li className="menu-desc">{item.description}</li> : null} {item.addons.map((addon,i)=>{return <li key={i} className="menu-addons">{addon.name} ${parseFloat(addon.price).toFixed(2)}</li>})}</ul>
        })
    }
    render(){
        return(
            <div className="menu-container">
                <h1 className="page-heading">Our Menu <a href="/menu.pdf" download="menu.pdf" className="menu-download"><FontAwesome name="file-pdf" ariaLabel="Download PDF Menu"/></a></h1>
                <p className="key">
                    <FontAwesome name="circle" className="gluten-free" /> - notes a gluten free item
                    <br/>
                    <FontAwesome name="times-circle" className="soy-free" /> - notes a soy free item
                </p>
                <h3 className="cat-type app">Appetizers</h3>
                <div className="cat-menu">
                    {this.renderMenuItemsByCategory('Appetizers')}
                </div>
                <h3 className="cat-type salad">Salads</h3>
                <p className="cat-addon">add grilled chik’n +3.00</p>
                <div className="cat-menu">
                    {this.renderMenuItemsByCategory('Salads')}
                </div>
                <h3 className="cat-type salad">Bowls</h3>
                <p className="cat-addon">add grilled chik’n +3.00, add setian +3.00</p>
                <div className="cat-menu bowls">
                    {this.renderMenuItemsByCategory('Bowls')}
                </div>
                <h3 className="cat-type">Breakfast</h3>
                <div className="cat-menu breakfast">
                    {this.renderMenuItemsByCategory('Breakfast')}
                </div>
                <h3 className="cat-type salad">Pancakes</h3>
                <p className="cat-addon">add whipped cream +2.00 <FontAwesome name="times-circle" className="soy-free"/></p>
                <div className="cat-menu">
                    {this.renderMenuItemsByCategory('Pancakes')}
                </div>
                <h3 className="cat-type salad">Sandwiches</h3>
                <p className="cat-addon">comes with side salad, get fries instead +4.00</p>
                <div className="cat-menu">
                    {this.renderMenuItemsByCategory('Sandwiches')}
                </div>
                <h3 className="cat-type salad">Burgers & Dogs</h3>
                <p className="cat-addon">served with fries & pickle spear</p>
                <div className="cat-menu">
                    {this.renderMenuItemsByCategory('Burgers & Dogs')}
                </div>
                <h3 className="cat-type">Shakes</h3>
                <div className="cat-menu">
                    {this.renderMenuItemsByCategory('Shakes')}
                </div>
                <h3 className="cat-type">Sundaes</h3>
                <div className="cat-menu">
                    {this.renderMenuItemsByCategory('Sundaes')}
                </div>
                <h3 className="cat-type salad">Vegan Treats</h3>
                <p className="cat-addon">available Wednesday thru Sunday while it lasts!</p>
                <div className="cat-menu">
                    {this.renderMenuItemsByCategory('Vegan Treats')}
                </div>
                <h3 className="cat-type">Drinks</h3>
                <div className="cat-menu">
                    {this.renderDrinkItems()}
                </div>
                <h3 className="cat-type">Sides & Extras</h3>
                <div className="cat-menu">
                    {this.renderMenuItemsByCategory('Sides & Extras')}
                </div>
            </div>
        );
    }
}