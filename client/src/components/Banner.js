import React,{Component} from 'react';
import champsLogo from '../images/champsLogo.jpeg';
import {Link} from 'react-router-dom';
import Auth from '../modules/Auth';

export default class Banner extends Component{
    constructor(){
        super();
        this.state = {
            isAuthenticated: false
        };
    }
    componentDidMount(){
        this.setState({isAuthenticated: Auth.isUserAuthenticated()});
    }
    componentWillUpdate(){
        if(this.state.isAuthenticated !== Auth.isUserAuthenticated()){
            this.setState({isAuthenticated: Auth.isUserAuthenticated()});
        }
    }
    render(){
        return(
            <header className="banner-wrapper">
                {this.state.isAuthenticated ? <Link to="/admin" className="admin-tools-link">Admin Tools</Link> : null}
                <Link to="/"><img src={champsLogo} alt="champs diner logo" /></Link>
                <p className="banner-address">197 Meserole Street, Brooklyn, NY 11206 | 718.599.2743 | info@champsdiner.com | Monday-Sunday 9am-12am</p>
            </header>
        );
    }
}

