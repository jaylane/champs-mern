import React from 'react';

const About = ()=>{
    return(
        <div className="about-wrapper">
           <h1 className="page-heading">About Champs Diner</h1>
           <p>Brooklyn’s favorite all-vegan diner, serving up Americana comfort food with a no-fuss, laid-back attitude.</p>
           <p className="line-height">Since opening its doors five years ago, Champs has become a beloved brunch and late night spot for those in the know. Stop by to see what everyone’s talking about - you’ll leave with a full belly and a smile on your face!</p>
           <p className="line-height">Place your order online through <a href="https://www.seamless.com/menu/champs-197-meserole-st-brooklyn/279740" target="_blank" rel="noopener noreferrer">Seamless</a> and <a href="https://www.grubhub.com/restaurant/champs-197-meserole-st-brooklyn/279740" target="_blank" rel="noopener noreferrer">Grubhub</a> (from 10AM-9.30PM). Or find us on <a href="https://primenow.amazon.com/restaurants/champs-diner-brooklyn/d/B01F2P0YL2?ref_=amzrst_b_B01F2P0YL2_11" target="_blank" rel="noopener noreferrer">Amazon Restaurants</a> (from 9AM-11.29PM)!</p>
        </div>
    );
}

export default About;