import React, {Component} from 'react';
import {NavLink,Route,Redirect} from 'react-router-dom';
import AddMenu from './AddMenu';
import EditMenu from './EditMenu';
var axios = require('axios');

export default class Admin extends Component{
    constructor(props){
        super(props);
        this.state = {categories:[],items:[]};
        this.getMenuItems = this.getMenuItems.bind(this);
        this.loadMenuCategories = this.loadMenuCategories.bind(this);
    }
    getMenuItems(){
        let url = '/api/menu/items';
        return axios.get(url);
    }
    loadMenuCategories(){
        let url = '/api/menu/categories';
        return axios.get(url);
    }
    fetchData = ()=>{
        axios.all([this.getMenuItems(),this.loadMenuCategories()])
        .then(axios.spread((items, cats)=>{
            this.setState({categories: cats.data.categories,items:items.data.items});
        }));
    }
    updateMenuCount = ()=>{
    this.getMenuItems()
        .then((items)=>{
            this.setState({items:items.data.items});
        });
    }
    componentDidMount(){
        this.fetchData();
    }
    render(){
        return(
            <div className="login-container">
                <h1 className="page-heading">Administrator Tools</h1>
                <ul className="sub-nav">
                    <li><NavLink activeClassName="sub-nav-active" to={`${this.props.match.url}/addItem`}>Add Menu Items</NavLink></li>
                    {this.state.items.length > 0 ? <li><NavLink activeClassName="sub-nav-active" to={`${this.props.match.url}/editMenu`}>Edit Menu ({this.state.items.length} Items)</NavLink></li> : null}
                </ul>
                <div className="admin-content">
                    <Route exact path={`${this.props.match.url}`} render={()=> <Redirect to={`${this.props.match.url}/addItem`} />} />
                    {this.state.items.length < 1 && window.location.pathname !== `${this.props.match.url}/addItem` ? <Redirect to={`${this.props.match.url}/addItem`} /> : null}
                    <Route path={`${this.props.match.url}/addItem`} render={()=><AddMenu categories={this.state.categories} menuCount={this.updateMenuCount} />} />
                    <Route path={`${this.props.match.url}/editMenu`} render={()=><EditMenu categories={this.state.categories} menuCount={this.updateMenuCount} items={this.state.items} />} />
                </div>
            </div>
        )
    }
}
