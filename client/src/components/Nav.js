import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';

export default class Nav extends Component{
    handleToggleClass = ()=>{
        var el = document.querySelector('.burger'),
        navWrap = document.querySelector('.nav-wrapper'),
        nav = document.querySelector('.main-nav');
        el.classList.toggle('open');
        nav.style.display === '' ? nav.style.display = 'flex' : nav.style.display = '';
        navWrap.classList.toggle('open-wrap');
    }
    render(){
        let burger = <div className="burger" onClick={this.handleToggleClass}><span></span><span></span><span></span></div>;

        return(
            <nav className="nav-wrapper">
                {burger}
                <ul className="main-nav">
                    <li><NavLink exact to="/about" activeClassName="nav-active">About</NavLink></li>
                    <li><NavLink exact to="/faq" activeClassName="nav-active">Faq</NavLink></li>
                    <li><NavLink exact to="/catering" activeClassName="nav-active">Catering</NavLink></li>
                    <li><NavLink exact to="/menu" activeClassName="nav-active">Menu</NavLink></li>
                    <li><NavLink exact to="/contact" activeClassName="nav-active">Contact</NavLink></li>
                    <li><a href="http://champsdiner.bigcartel.com/" target="_blank" rel="noopener noreferrer">Store</a></li>
                </ul>
            </nav>
        )
    }
}