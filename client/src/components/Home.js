import React from 'react';
import Instagram from './Instagram';

const Home = ()=>{
    return(
        <div className="home-container">
            <h3 className="home-heading">Welcome to ChampsDiner.com</h3>
            <Instagram count={12}></Instagram>
        </div>
    )
}

export default Home;