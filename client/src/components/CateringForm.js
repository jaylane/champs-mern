import React, {Component} from 'react';
import 'whatwg-fetch';

export default class CateringForm extends Component{
    constructor(){
        super();
        this.state = {
            showForm: true
        }
    }
    handleSubmit = (e)=>{
        e.preventDefault();
        let name = this.name.value,
        email = this.email.value,
        message = this.message.value,
        phone = this.tel.value,
        eventType = this.eventType.value,
        eventSize = this.eventSize.value,
        eventAddress = this.eventAddress.value,
        eventDate = this.eventDate.value,
        eventMethod = this.eventMethod.value;

        fetch('/api/catering', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              name,email,message,phone,eventType,eventSize,eventAddress,eventDate,eventMethod
            })
        });
        this.form.reset();
        this.setState({...this.state,showForm:false});
    }
    render(){
        let body;
        if(this.state.showForm){
            body = [<p key={1} className="flex-center cater">Send a message to our catering coordinator to bring Champs to your next event!</p>,
            <form key={2} className="contact-form" ref={(form)=>{this.form = form}}>
            <ul className="flex-outer cater">
                <li className="name-item first-group">
                    <label htmlFor="name">Name:</label>
                    <input autoComplete="name" type="text" name="name" ref={(input)=> this.name = input} required />    
                </li>
                <li className="email-item first-group">
                    <label htmlFor="email">Email:</label>
                    <input autoComplete="email" type="email" name="email" ref={(input)=> this.email = input} required />  
                </li>
                <li className="tel-item first-group">
                    <label htmlFor="tel">Phone:</label>
                    <input autoComplete="tel-national" type="tel" name="tel" ref={(input)=> this.tel = input} required />  
                </li>
                <li className="event-type-item">
                    <label htmlFor="event-type">Type of Event:</label>
                    <input autoComplete="nope" type="text" name="event-type" ref={(input)=> this.eventType = input} required />    
                </li>
                <li className="event-address-item">
                    <label htmlFor="event-type">Address of Event:</label>
                    <input autoComplete="nope" type="text" name="event-address" ref={(input)=> this.eventAddress = input} required />    
                </li>
                <li>
                    <label htmlFor="event-size">Number of Guests:</label>
                    <input autoComplete="nope" type="number" name="event-size" ref={(input)=> this.eventSize = input} required />    
                </li>
                <li>
                    <label htmlFor="event-date">Event Date & Time:</label>
                    <input autoComplete="nope" type="date" name="event-date" ref={(input)=> this.eventDate = input} required />    
                </li>
                <li>
                <label htmlFor="event-method">Delivery or Pickup:</label>
                    <select name="event-method" required ref={(input)=> this.eventMethod = input}>
                        <option value="Delivevry" defaultValue>Delivery</option>
                        <option value="Pick Up">Pick Up</option>
                    </select>  
                </li>
                <li className="message-item">
                    <label htmlFor="message">Special Notes:</label>
                    <textarea rows="6" type="textarea" name="message" ref={(input)=> this.message = input} required />
                </li>
                <li>
                    <button type="submit" onClick={this.handleSubmit}>Submit</button>
                </li>
            </ul>
        </form>];
        }else{
            body = body = <h3 className="contact-thanks">Thanks! We'll get back to you as soon as possible.</h3>
        }
        return(
            <div className="contact-wrapper">
            <h1 className="page-heading">Catering Request</h1>
                {body}
            </div>
        )
    }
}