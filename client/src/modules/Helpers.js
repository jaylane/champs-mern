module.exports = {
    menu:{
        stringifyAddons: function(addonsArray){
            let stringifiedAddons;
            try{
                stringifiedAddons = addonsArray.map((el)=>{return el.name + ':' + parseFloat(el.price).toFixed(2)}).join(',');
                return stringifiedAddons;
            }catch(e){
                console.error(e);
                return e;
            }
        },
        destructureAddons: function(addonsString){
            let addonsArray;
            try{
                addonsArray = addonsString.split(',').map((e)=>{return e.split(':')});
                return addonsArray;
            }catch(e){
                console.error(e);
                return e;
            }
        }
    }
};