import AddMenu from '../components/AddMenu';
import {updateMenuCount} from '../components/Admin';
import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme,{mount} from 'enzyme';
Enzyme.configure({ adapter: new Adapter() })

const initialCats = [{_id:0,name:'Apps'},{_id:1,name:'Breakfast'}];
const wrapper = mount(<AddMenu categories={initialCats} menuCount={updateMenuCount} />);
const formWrap = wrapper.find('.about-wrapper');
const form = wrapper.find('.add-menu-item-form');

test('AddMenu component should render the add menu item form',()=>{
    expect(formWrap.html()).toContain(form.html());
});

test('Menu form should have drop down containing category names',()=>{
    const catSelect = wrapper.find('select[name="type"]')
    expect(formWrap.html()).toContain()
});