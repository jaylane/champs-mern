//module to create email template with supplied form data
module.exports.createEmail = ({name,email,message})=>
`<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width"/>

    <!-- For development, pass document through inliner -->

    <style type="text/css">

    * {
        margin: 0;
        padding: 0;
        font-size: 100%;
        font-family: 'Avenir Next', "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
        line-height: 1.65; }
      
      img {
        max-width: 100%;
        margin: 0 auto;
        display: block; }
      
      body,
      .body-wrap {
        width: 100% !important;
        height: 100%;
        background: #efefef;
        -webkit-font-smoothing: antialiased;
        -webkit-text-size-adjust: none; }
      
      a {
        color: #black;
        text-decoration: none; }
      
      .text-center {
        text-align: center; }
      
      .text-right {
        text-align: right; }
      
      .text-left {
        text-align: left; }
      
      .button {
        display: inline-block;
        color: white;
        background: #71bc37;
        border: solid #71bc37;
        border-width: 10px 20px 8px;
        font-weight: bold;
        border-radius: 4px; }
      
      h1, h2, h3, h4, h5, h6{
        margin-bottom: 20px;
        line-height: 1.25; }
      
      h1 {
        font-size: 30px; }
      
      h2 {
        font-size: 28px;padding-top:40px;}
      
      h3 {
        font-size: 24px; }
      
      h4 {
        font-size: 20px; }
      
      h5 {
        font-size: 16px; }
      
      p, ul, ol {
        font-size: 16px;
        font-weight: normal;
        margin-bottom: 20px; }
      
      .container {
        display: block !important;
        clear: both !important;
        margin: 0 auto !important;
        max-width: 580px !important; }
        .container table {
          width: 100% !important;
          border-collapse: collapse; }
        .container .masthead {
          padding-top: 50px;
          background: white;
          color: black; }
          .container .masthead h1 {
            margin: 0 auto !important;
            max-width: 90%;
            text-transform: uppercase;}
        .container .content {
          background: white;
          padding: 30px 35px; }
          .container .content.footer {
            background: none; }
            .container .content.footer p {
              margin-bottom: 0;
              color: #888;
              text-align: center;
              font-size: 14px; }
            .container .content.footer a {
              color: #888;
              text-decoration: none;
              font-weight: bold; }
            

    </style>
</head>
<body>
<table class="body-wrap">
    <tr>
        <td class="container">

            <!-- Message start -->
            <table>
                <tr>
                    <td align="center" class="masthead">
                        <img src="http://beta.champsdiner.com/static/media/champsLogo.46192b3d.jpeg" alt="champs diner logo" width="70%">
                        <h2>Contact Form Submission</h2>

                    </td>
                </tr>
                <tr>
                    <td class="content">

                        <h4>Name:</h4>
                        <p>${name}</p>
                        <h4>Email:</h4>
                        <p>${email}</p>
                        <h4>Message:</h4>
                        <p>${message}</p>
                    </td>
                </tr>
                <tr>
                <td class="container">
        
                    <!-- Message start -->
                    <table>
                        <tr>
                            <td class="content footer" align="center">
                                <p>Sent by <a href="https://www.champsdiner.com">ChampsDiner.com</a></p>
                            </td>
                        </tr>
                    </table>
        
                </td>
            </tr>
            </table>

        </td>
    </tr>
</table>
</body>
</html>`;

//module to create email template with supplied form data
module.exports.createCateringEmail = ({name,email,message,phone,eventType,eventSize,eventAddress,eventDate,eventMethod})=>
`<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width"/>

    <!-- For development, pass document through inliner -->

    <style type="text/css">

    * {
        margin: 0;
        padding: 0;
        font-size: 100%;
        font-family: 'Avenir Next', "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
        line-height: 1.65; }
      
      img {
        max-width: 100%;
        margin: 0 auto;
        display: block; }
      
      body,
      .body-wrap {
        width: 100% !important;
        height: 100%;
        background: #efefef;
        -webkit-font-smoothing: antialiased;
        -webkit-text-size-adjust: none; }
      
      a {
        color: #black;
        text-decoration: none; }
      
      .text-center {
        text-align: center; }
      
      .text-right {
        text-align: right; }
      
      .text-left {
        text-align: left; }
      
      .button {
        display: inline-block;
        color: white;
        background: #71bc37;
        border: solid #71bc37;
        border-width: 10px 20px 8px;
        font-weight: bold;
        border-radius: 4px; }
      
      h1, h2, h3, h4, h5, h6{
        margin-bottom: 20px;
        line-height: 1.25; }
      
      h1 {
        font-size: 30px; }
      
      h2 {
        font-size: 28px;padding-top:40px;}
      
      h3 {
        font-size: 24px; }
      
      h4 {
        font-size: 20px; }
      
      h5 {
        font-size: 16px; }
      
      p, ul, ol {
        font-size: 16px;
        font-weight: normal;
        margin-bottom: 20px; }
      
      .container {
        display: block !important;
        clear: both !important;
        margin: 0 auto !important;
        max-width: 580px !important; }
        .container table {
          width: 100% !important;
          border-collapse: collapse; }
        .container .masthead {
          padding-top: 50px;
          background: white;
          color: black; }
          .container .masthead h1 {
            margin: 0 auto !important;
            max-width: 90%;
            text-transform: uppercase;}
        .container .content {
          background: white;
          padding: 30px 35px; }
          .container .content.footer {
            background: none; }
            .container .content.footer p {
              margin-bottom: 0;
              color: #888;
              text-align: center;
              font-size: 14px; }
            .container .content.footer a {
              color: #888;
              text-decoration: none;
              font-weight: bold; }
            

    </style>
</head>
<body>
<table class="body-wrap">
    <tr>
        <td class="container">

            <!-- Message start -->
            <table>
                <tr>
                    <td align="center" class="masthead">
                        <img src="http://beta.champsdiner.com/static/media/champsLogo.46192b3d.jpeg" alt="champs diner logo" width="70%">
                        <h2>Catering Request Form Submission</h2>

                    </td>
                </tr>
                <tr>
                    <td class="content">

                        <h4>Name:</h4>
                        <p>${name}</p>
                        <h4>Email:</h4>
                        <p>${email}</p>
                        <h4>Phone:</h4>
                        <p>${phone}</p>
                        <h4>Event Type:</h4>
                        <p>${eventType}</p>
                        <h4>Address:</h4>
                        <p>${eventAddress}</p>
                        <h4>Date:</h4>
                        <p>${eventDate}</p>
                        <h4>Number of Guests:</h4>
                        <p>${eventSize}</p>
                        <h4>Delivery or Pickup:</h4>
                        <p>${eventMethod}</p>
                        <h4>Special Notes:</h4>
                        <p>${message}</p>
                    </td>
                </tr>
                <tr>
                <td class="container">
        
                    <!-- Message start -->
                    <table>
                        <tr>
                            <td class="content footer" align="center">
                                <p>Sent by <a href="https://www.champsdiner.com">ChampsDiner.com</a></p>
                            </td>
                        </tr>
                    </table>
        
                </td>
            </tr>
            </table>

        </td>
    </tr>
</table>
</body>
</html>`;